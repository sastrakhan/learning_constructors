﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventPlanning
{
    class DinnerParty
    {
        public int NumberOfPeople {get; set;}
        public bool FancyDecorations {get; set;}
        public bool HealthyOption { get; set; }

        public DinnerParty (int numberOfPeople, bool healthyOption, bool fancyDecorations){
      //      this.NumberOfPeople = numberOfPeople;
      //      this.HealthyOption = healthyOption;
      //      this.FancyDecorations = fancyDecorations;
        }

         private decimal CalculateCostOfDecorations(bool DecorationsChecked)
        {
            decimal costOfDecorations;
            if (FancyDecorations) {
                costOfDecorations = 15.0M * NumberOfPeople + 50.0M;
            }
            else
            {
                costOfDecorations = 7.50M * NumberOfPeople + 30.0M;
            }
            return costOfDecorations;
        }

         private decimal CalculateCostOfBeveragesPerPerson()
         {
             decimal costOfBeverages;
             if (HealthyOption)
             {
                 costOfBeverages = 5 * NumberOfPeople;
             }
             else
             {
                 costOfBeverages = 20 * NumberOfPeople;
             }
             return costOfBeverages;
         }

        public decimal Cost{
            get{
                decimal totalCost;
                totalCost = CalculateCostOfBeveragesPerPerson() + CalculateCostOfDecorations(FancyDecorations) + (NumberOfPeople * 25);
                return totalCost;
            }

        }

    }
}
